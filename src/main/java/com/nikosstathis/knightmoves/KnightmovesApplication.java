package com.nikosstathis.knightmoves;

import com.nikosstathis.knightmoves.service.IOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is the entry point of the application
 */
@SpringBootApplication
public class KnightmovesApplication implements CommandLineRunner {

    @Autowired
    IOService ioService;

    public static void main(String... args) {
        SpringApplication app = new SpringApplication(KnightmovesApplication.class);
        //a shutdown hook is necessary because we will use the Scanner.class and without it the
        //program might not stop using Ctrl+C
        Runtime.getRuntime().addShutdownHook(new Thread(() -> Thread.currentThread().interrupt()));
        app.run();
    }

    @Override
    public void run(String... args) throws Exception {
        ioService.handleIO(); //call the IOService that is responsible for the message processing
    }

}
