package com.nikosstathis.knightmoves.service;

import org.springframework.stereotype.Service;

/**
 * IOService is the interface that defines all the methods needed for our service
 * that handles the input/output of the program
 */
@Service
public interface IOService {

    /**
     * Displays an initial message to the user that he can start sending request.
     * Then reads from the console the requests, sends each request to be processed and displays to
     * the user the results of the request processing
     */
    void handleIO();

    /**
     * handles request, sends it to be processed and returns the output to the user.
     * The result could be either a solution or an exception message
     * @param request is the last user actual request of the user containing the starting and the ending positions in
     * chess notation
     * @return the message that will be displayed to the user
     */
    String handleNextRequest(String request);

}
