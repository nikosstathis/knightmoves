package com.nikosstathis.knightmoves.service;

import com.nikosstathis.knightmoves.bean.Position;
import com.nikosstathis.knightmoves.error.NoSolutionException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ChessService is the interface that defines all the methods needed for our service
 * that handles the chess board and the conversions ftom chess notations to Positions
 * and vice-versa
 */
@Service
public interface ChessService {

    /**
     * given a List of Positions, returns the String that will be displayed to the user
     * e.g. for a list containing Position(1, 1) , Position(3, 2), Position(5, 3) it would return A1 -> C2 -> E3
     * @param moves defines the given list of Positions
     * @return the String the user is going to see as a solution
     */
    String movesToString(List<Position> moves);

    /**
     * given a Position, returns the chess notation e.g. for Position(1, 1) it will return A1
     * @param p defines the given Position
     * @return the chess notation for this position
     */
    String getChessNotationFromPosition(Position p);

    /**
     * given a chess notation, returns the Position, e.g. for chess notation A1 it will return Position(1, 1)
     * @param notation defines the given chess notation
     * @return the position for this chess notation
     */
    Position getPositionFromChessNotation(String notation);

    /**
     * indicates if a Position is valid (inside the board ) or not
     * @param p defines the Position that we want to examine if it is inside the board or not
     * @return true when the position is valid, false otherwise
     */
    boolean isValidPosition(Position p);

    /**
     * calculates the knight's route to go from point 'from' to point 'to'
     * @param from is the chess notation that defines the starting position
     * @param to is the chess notation that defines the ending position
     * @return a string that contains the route
     * @throws NoSolutionException when no solution could be found for this combination of positions
     */
    String findKnightRoute(String from, String to) throws NoSolutionException;

}
