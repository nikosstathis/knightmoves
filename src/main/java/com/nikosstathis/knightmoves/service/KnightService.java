package com.nikosstathis.knightmoves.service;

import com.nikosstathis.knightmoves.bean.Position;
import com.nikosstathis.knightmoves.error.NoSolutionException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * KnightService is the interface that defines all the methods needed for our service
 * that handles the calculation of the knight's route
 */
@Service
public interface KnightService {

   /**
    * given the starting and the ending Position, returns the route that the knight should follow
    * @param from defines the starting Position
    * @param to defines the ending Position
    * @return the list of the Positions that the knight should follow (the first one is always the 'from', the last
    * one is always the 'to')
    * @throws NoSolutionException when the algorithm could not find a path
    */
   List<Position> findKnightRoute(Position from, Position to) throws NoSolutionException;

}
