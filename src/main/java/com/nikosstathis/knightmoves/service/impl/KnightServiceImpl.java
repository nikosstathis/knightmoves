package com.nikosstathis.knightmoves.service.impl;

import com.nikosstathis.knightmoves.bean.Position;
import com.nikosstathis.knightmoves.error.NoSolutionException;
import com.nikosstathis.knightmoves.service.ChessService;
import com.nikosstathis.knightmoves.service.KnightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class KnightServiceImpl implements KnightService {

    @Value("${max_moves}")
    private int maxMoves; //the value is read from the application.properties file

    @Autowired
    ChessService chessService;

    private final List<Position> availableMoves = Arrays.asList(
            new Position(2, 1),
            new Position(2, -1),
            new Position(1, 2),
            new Position(1, -2),
            new Position(-2, 1),
            new Position(-2, -1),
            new Position(-1, 2),
            new Position(-1, -2)
    );

    private List<Position> findMovesBreadthFirstSearch(List<LinkedList<Position>> routeList, Position finalPosition) {
        List<LinkedList<Position>> nextRouteList = new ArrayList<>();
        for (LinkedList<Position> route : routeList) {
            Position lastPosition = route.peekLast();
            if (route.size() <= maxMoves) { // calculate all the available routes so as to iterate them
                for (Position move : availableMoves) {
                    Position newPosition = new Position(lastPosition.getX() + move.getX(), lastPosition.getY() + move.getY());
                    if (chessService.isValidPosition(newPosition)) { //discard positions outside of the board
                        LinkedList<Position> newRoute = new LinkedList<>(route);
                        newRoute.add(newPosition);
                        if (newPosition.equals(finalPosition)) { //found the desired result, can return now
                            return newRoute;
                        } else {
                            nextRouteList.add(newRoute);
                        }
                    }
                }
            }
        }
        if (nextRouteList.isEmpty()) { // this is the case where no results were found but no  more moves could be calculated
            return Collections.emptyList();
        } else { //in this case we found some more moves we could test, will do recursion for this
            return findMovesBreadthFirstSearch(nextRouteList, finalPosition);
        }
    }

    @Override
    public List<Position> findKnightRoute(Position from, Position to) throws NoSolutionException {
        if(from.equals(to)){
            return Arrays.asList(from);
        }
        List<Position> result = findMovesBreadthFirstSearch(Arrays.asList(new LinkedList<>(Arrays.asList(from))), to);
        if (result.isEmpty()) {
            throw new NoSolutionException();
        }
        return result;
    }

}
