package com.nikosstathis.knightmoves.service.impl;

import com.nikosstathis.knightmoves.service.ChessService;
import com.nikosstathis.knightmoves.service.IOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
public class IOServiceImpl implements IOService {

    @Autowired
    ChessService chessService;

    private static final String SYNTAX_MSG = "Give From and To Knight positions, e.g. \"A1 C2\" and press ENTER";

    @Override
    public void handleIO() {
        System.out.println(SYNTAX_MSG);
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine() && !Thread.currentThread().isInterrupted()) {
            System.out.println(handleNextRequest(scanner.nextLine()) + "\nnext request:");
        }
    }

    @Override
    public String handleNextRequest(String request) {
        String[] args = request.trim().split(" ");
        String message = null;
        try {
            if (args.length == 2) {
                message = chessService.findKnightRoute(args[0], args[1]);
            } else {
                message = "Invalid Number of Arguments: " + SYNTAX_MSG;
            }
        } catch (Exception ex) {
            message = ex.getMessage();
        }
        return message;
    }

}
