package com.nikosstathis.knightmoves.service.impl;

import com.nikosstathis.knightmoves.bean.Position;
import com.nikosstathis.knightmoves.error.NoSolutionException;
import com.nikosstathis.knightmoves.service.ChessService;
import com.nikosstathis.knightmoves.service.KnightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ChessServiceImpl implements ChessService {

    private static final String CHESS_NOTATION_REGEX = "[A-H][1-8]";
    private static final int X_BOARD_SIZE = 8;
    private static final int Y_BOARD_SIZE = 8;

    @Autowired
    KnightService knightService;

    Map<Integer, String> xAxisToNotation = Stream.of(
            new AbstractMap.SimpleEntry<>(1, "A"),
            new AbstractMap.SimpleEntry<>(2, "B"),
            new AbstractMap.SimpleEntry<>(3, "C"),
            new AbstractMap.SimpleEntry<>(4, "D"),
            new AbstractMap.SimpleEntry<>(5, "E"),
            new AbstractMap.SimpleEntry<>(6, "F"),
            new AbstractMap.SimpleEntry<>(7, "G"),
            new AbstractMap.SimpleEntry<>(8, "H"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    @Override
    public String movesToString(List<Position> moves) {
        return moves.stream()
                .map(this::getChessNotationFromPosition)
                .collect(Collectors.joining(" -> "));
    }

    @Override
    public String getChessNotationFromPosition(Position p) {
        return xAxisToNotation.get(p.getX()) + p.getY();
    }

    @Override
    public Position getPositionFromChessNotation(String notation) {
        if (!notation.matches(CHESS_NOTATION_REGEX)) {
            throw new IllegalArgumentException("Invalid Chess Notation: " + notation);
        }
        final Integer x = xAxisToNotation.entrySet().stream()
                .filter(e -> e.getValue().equals(notation.substring(0, 1)))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new)
                .getKey();
        return new Position(x, Integer.valueOf(notation.substring(1)));
    }

    public boolean isValidPosition(Position p) {
        return p.getX() > 0 && p.getY() > 0 && p.getX() <= X_BOARD_SIZE && p.getY() <= Y_BOARD_SIZE;
    }


    @Override
    public String findKnightRoute(String from, String to) throws NoSolutionException {
        final Position fromPosition = getPositionFromChessNotation(from);
        final Position toPosition = getPositionFromChessNotation(to);

        final List<Position> moves = knightService.findKnightRoute(fromPosition, toPosition);
        return movesToString(moves);
    }

}
