package com.nikosstathis.knightmoves.bean;

import lombok.*;

/**
 * This bean is used to indicate the squares of the chess board
 * x is the x axis
 * y is the y axis
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Position {
    int x;
    int y;
}
