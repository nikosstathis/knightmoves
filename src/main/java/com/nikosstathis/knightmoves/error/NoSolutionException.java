package com.nikosstathis.knightmoves.error;

/**
 * This exception is used when no route that connects the 2 positions exist
 */
public class NoSolutionException extends Exception {
    public NoSolutionException() {
        super("No route could be found for this input");
    }
}
