package com.nikosstathis.knightmoves.service;

import com.nikosstathis.knightmoves.bean.Position;
import com.nikosstathis.knightmoves.error.NoSolutionException;
import com.nikosstathis.knightmoves.service.impl.ChessServiceImpl;
import com.nikosstathis.knightmoves.service.impl.KnightServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * KnightServiceTestCase provides tests for the KnightService implementation
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {KnightServiceImpl.class, ChessServiceImpl.class})
@TestPropertySource("classpath:application.properties")
public class KnightServiceTestCase {

    @Autowired
    KnightService knightService;

    @Test
    public void testKnightRoute() throws NoSolutionException {

        List<Position> expectedRoute = Arrays.asList(new Position(1, 1), new Position(3, 2), new Position(5, 3), new Position(7, 4));
        List<Position> foundRoute = knightService.findKnightRoute( new Position(1, 1), new Position(7, 4));
        assertEquals(expectedRoute, foundRoute);

        expectedRoute = Arrays.asList(new Position(1, 1), new Position(2, 3), new Position(3, 5), new Position(4, 7));
        foundRoute = knightService.findKnightRoute(new Position(1, 1), new Position(4, 7));
        assertEquals(expectedRoute, foundRoute);

        expectedRoute = Arrays.asList(new Position(2, 2));
        foundRoute = knightService.findKnightRoute(new Position(2, 2), new Position(2, 2));
        assertEquals(expectedRoute, foundRoute);

    }

}
