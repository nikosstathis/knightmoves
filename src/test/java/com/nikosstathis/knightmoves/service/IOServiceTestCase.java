package com.nikosstathis.knightmoves.service;

import com.nikosstathis.knightmoves.service.impl.ChessServiceImpl;
import com.nikosstathis.knightmoves.service.impl.IOServiceImpl;
import com.nikosstathis.knightmoves.service.impl.KnightServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * IOServiceTestCase provides tests for the IOService implementation
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {IOServiceImpl.class,
        KnightServiceImpl.class, ChessServiceImpl.class})
@TestPropertySource("classpath:application.properties")
public class IOServiceTestCase {

    @Autowired
    IOService ioService;

    @Test
    public void testIO() {
        assertEquals("A1 -> C2", ioService.handleNextRequest("A1 C2"));
        assertEquals("Invalid Number of Arguments: Give From and To Knight positions, e.g. \"A1 C2\" " +
                "and press ENTER", ioService.handleNextRequest("A1 C2 C3"));
        assertEquals("No route could be found for this input", ioService.handleNextRequest("A1 G8"));

        ioService.handleNextRequest("A1 C2 B3");
    }

}
