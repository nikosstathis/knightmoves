package com.nikosstathis.knightmoves.service;

import com.nikosstathis.knightmoves.bean.Position;
import com.nikosstathis.knightmoves.error.NoSolutionException;
import com.nikosstathis.knightmoves.service.impl.ChessServiceImpl;
import com.nikosstathis.knightmoves.service.impl.KnightServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * ChessServiceTestCase provides tests for the ChessService implementation
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ChessServiceImpl.class, KnightServiceImpl.class})
@TestPropertySource("classpath:application.properties")
public class ChessServiceTestCase {

    @Autowired
    ChessService chessService;

    @Test
    public void testChessNotationFromPosition() {
        assertEquals("A1", chessService.getChessNotationFromPosition(new Position(1, 1)));
        assertEquals("H8", chessService.getChessNotationFromPosition(new Position(8, 8)));
        assertEquals("D5", chessService.getChessNotationFromPosition(new Position(4, 5)));
    }

    @Test
    public void testPositionFromChessNotation() {
        assertEquals(new Position(2, 3), chessService.getPositionFromChessNotation("B3"));
        assertEquals(new Position(8, 7), chessService.getPositionFromChessNotation("H7"));
        assertEquals(new Position(1, 1), chessService.getPositionFromChessNotation("A1"));
    }

    @Test
    public void testPositionFromInvalidNotation() {
        assertThrows(IllegalArgumentException.class, () -> chessService.getPositionFromChessNotation("A9"));
    }

    @Test
    public void testValidPosition() {
        assertTrue(chessService.isValidPosition(new Position(1, 3)));
        assertFalse(chessService.isValidPosition(new Position(0, 3)));
        assertFalse(chessService.isValidPosition(new Position(0, 3)));
        assertFalse(chessService.isValidPosition(new Position(3, 0)));
        assertFalse(chessService.isValidPosition(new Position(1, 9)));
        assertFalse(chessService.isValidPosition(new Position(9, 1)));
    }

    @Test
    public void testMovesToString() {
        assertEquals(
                "A1 -> C2 -> E3",
                chessService.movesToString(
                        Arrays.asList(new Position(1, 1), new Position(3, 2), new Position(5, 3))
                ));
    }

    @Test
    public void testKnightRoute() throws NoSolutionException {
        assertEquals("A1 -> C2 -> E3 -> G4", chessService.findKnightRoute("A1", "G4"));
    }

    @Test
    public void testKnightRouteIlleganArgumentsException() throws NoSolutionException {
        assertThrows(IllegalArgumentException.class, () -> chessService.findKnightRoute("A9", "G4"));
    }

    @Test
    public void testKnightRouteNoSolutionException() throws NoSolutionException {
        assertThrows(NoSolutionException.class, () -> chessService.findKnightRoute("A1", "H8"));
    }

}
