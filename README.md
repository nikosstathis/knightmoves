**1. Introduction**

This is console application that calculates the shortest path a knight can take on an empty chessboard to get from a starting to an ending position

**2. Architecture**
- This is a Spring Boot application.
- It runs on Java 8 or newer versions of Java
- It is a maven project
- Spring is used for dependency injection
- Lombok is used to generate methods for the POJOs
- Standard input/output is used as an interface with the user

**3. Building and Running the Application**
- Clone the code from the git repository

`git clone https://gitlab.com/nikosstathis/knightmoves.git`

- Go to the knightmoves directory

`cd knightmoves`

- Build the application using maven

`mvn clean install -DskipTests`

- Run the application

`java -jar target\knightmoves-0.0.1.jar`

If everything has gone well you should see this message:
>Give From and To Knight positions, e.g. "A1 C2" and press ENTER

**4. Configuring the Application**
The maximum number of moves of the knight is configurable. You can edit it in the application.properties file. The default value is 3.

**5. Testing the Application**
You can test the application by giving the starting position of the knight and the ending position in chess notation.
If the application can find a solution, it will display it on the screen.
It will also display errors on these occasions:
- if there are more than 2 arguments
- if there chess notation of a position is wrong
- if no solution can be found in 3 moves

Here are some test cases:

>>>
java -jar target\knightmoves-0.0.1.jar

Give From and To Knight positions, e.g. "A1 C2" and press ENTER

A1 V1

Invalid Chess Notation: V1

next request:

A1 A2 A3

Invalid Number of Arguments: Give From and To Knight positions, e.g. "A1 C2" and press ENTER

next request:

A1 G8

No route could be found for this input

next request:

A1 A2

A1 -> C2 -> B4 -> A2

next request:

A2 E7

A2 -> C3 -> D5 -> E7

next request:

B3 B3

B3

next request:

E8 G4

E8 -> F6 -> G4

next request:

A1 B3

A1 -> B3

next request:
>>>

**6. The code**

The core of the code are the 3 services:
- The IOService handles all the user's input and the application's output
- The KnightService calculates the desired path
- The ChessService acts as an orchestrator between  the IOService and the KnightService

All the services are defined as interfaces and have implementations and unit tests

**6. The knight algorithm**

The algorithm that calculates the knights moves is implemented in the KnightServiceImpl class.
- The availableMoves list contains all the 8 moves a knight can do every time.
- If the from/to positions are the same, we return a list containing this position
- When the from/to positions differ, we use breadth first search to calculate the shortest path
- A LinkedList of Positions contains a possible route that could lead to the destination
- For each of the possible routes, if there are more available moves, we calculate the 8 possibles routes the knight can make and then we filter out the moves that lead the knight outside of the board. If any of these positions is the desired destination, we return this path. If this is not the case, we have now 8-minus-the-invalid-moves paths that could lead to the destination. We use recursion to do this.
- findMovesBreadthFirstSearch is the recursive function. The argument
>List<LinkedList<Position>> routeList

is a list containing all the routes that could  lead to the  destination


